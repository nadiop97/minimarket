class User {
  constructor(budget) {
    this.budget = budget
  }

  checkBudget(cart) {
    if(this.budget >= cart.total && cart.total !== 0) {
      return true
    }
    return false
  }
}

// const user1 = new User(10)
// console.log(user1)