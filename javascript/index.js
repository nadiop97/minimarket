//Objects's instanciation
const newInventory = new Inventory()
let newCart = new Cart()
const newUser = new User(10)
let articleSelectedPosition = 0


//DOM's modification
document.addEventListener('DOMContentLoaded', () => {

  //init Inventory to Sart Select Article in Inventory Table
  reloadInventory()

  // Add Article Selected to Cart By Button "Ajouter"
  addArticleByButton()

  // Delete Article from Cart by Button "Supprimer"
  deleteArticleByButton()

  // Pay Total by Button "Payer" 
  payTotal()
})



// Complementaries functions 

  //INVENTORY FUNCTIONS
    //This function reload and fill dynamically the inventory table
      function reloadInventory(){
      const tbodyInventory = document.getElementById('tbody-inventory')      
      tbodyInventory.innerHTML=""
      newInventory.articles.forEach(FillingInventoryTable);
      clickArticleInventory()
    }

    //This function fill 1 row and 3 cols of the INVENTORY table
    function FillingInventoryTable(article) {
      const tbodyInventory = document.getElementById('tbody-inventory')      
      const trInventory = document.createElement('tr')
      const tdInventory = document.createElement('td')
      const tdInventory2 = document.createElement('td')
      const tdInventory3 = document.createElement('td')


      tbodyInventory.appendChild(trInventory)

      trInventory.classList.add("article-inventory")
      trInventory.appendChild(tdInventory)
      trInventory.appendChild(tdInventory2)
      trInventory.appendChild(tdInventory3)

      tdInventory.innerHTML = article.name
      tdInventory2.innerHTML= article.quantity
      tdInventory3.innerHTML= article.price+" €"
    }

    //This function select an Article on the Inventory and active the btn-add-article
    function clickArticleInventory() {
      const trInventoryArticles = document.getElementsByClassName('article-inventory')
      const btnAddArticle = document.getElementById('btn-add-article')
  
      btnAddArticle.disabled = true;
    
      for(let index = 0 ; index<trInventoryArticles.length; index++){
        trInventoryArticles[index].addEventListener('click',()=>{
          for(let article of trInventoryArticles) {
            article.classList.remove("bg-info")
          }
          btnAddArticle.disabled = false ;
          trInventoryArticles[index].classList.add("bg-info")
          articleSelectedPosition = index
        })
      }
    }

    // This functin ADD ARTICLE FROM INVENTORY TO CART BY BUTTON ADD
    function addArticleByButton(){
      const btnAddArticle = document.getElementById('btn-add-article')
      const totalRaise = document.getElementById('total')

      btnAddArticle.addEventListener('click',()=> {
        let index = articleSelectedPosition
        if(newInventory.articles[index].quantity > 0) {
          newCart.addArticle(newInventory.articles[index])
          newCart.articles.forEach(FillCartTable)
          newInventory.deleteQuantity(newInventory.articles[index])
          totalRaise.innerHTML = newCart.total

          reloadCart()
          reloadInventory()

          console.log("article checked : ", newInventory.articles[index].name)
          console.log("newquantity in inventory = ", newInventory.articles[index].quantity)
          console.log("total = ", newCart.total)
        } else { alert("Désolé! cet article est indisponible") }
      })
    }





  //CART FUNCTIONS
  
    //This function reload and fill dynamically the CART table 
    function reloadCart(){
      const tbodyCart = document.getElementById('tbody-cart')  

      tbodyCart.innerHTML=""
      newCart.articles.forEach(FillCartTable);
      clickArticleCart()
    }
  
    //This function fill 1 row and 2 cols of the cart table 
    function FillCartTable(article) {
      const tbodyCart = document.getElementById('tbody-cart')  
      const trCart = document.createElement('tr')
      const tdCart = document.createElement('td')
      const tdCart2 = document.createElement('td')

      tbodyCart.appendChild(trCart)

      trCart.classList.add("article-cart")
      trCart.appendChild(tdCart)
      trCart.appendChild(tdCart2)

      tdCart.innerHTML = article.name
      tdCart2.innerHTML= article.price+" €"
    }
    
    //This function Select an Article on The Cart and active btn-remove-article
    function clickArticleCart() {
      const trCartArticles = document.getElementsByClassName('article-cart')
      const btnRemoveArticle = document.getElementById('btn-remove-article')
  
      btnRemoveArticle.disabled = true;
    
      for(let index = 0 ; index<trCartArticles.length; index++){
        trCartArticles[index].addEventListener('click',()=>{
          for(let article of trCartArticles) {
            article.classList.remove("bg-warning")
          }
          btnRemoveArticle.disabled = false ;
          trCartArticles[index].classList.add("bg-warning")
          articleSelectedPosition = index
        })
      }
    }
    
    //This function DELETE ARTICLE FROM CART ADD RELOAD QUANTITY TO INVENTORY
    function deleteArticleByButton() {
      const btnRemoveArticle = document.getElementById('btn-remove-article')
      const totalDecrease = document.getElementById('total')

      btnRemoveArticle.addEventListener('click',()=> {
        let index = articleSelectedPosition
        let articleDelete = newCart.articles[index]

        newInventory.addQuantity(articleDelete)
        console.log("Tavant", newCart.total)
        newCart.deleteArticle(articleDelete,index)
        console.log("Tapres",newCart.total)
        newCart.articles.forEach(FillCartTable)
        totalDecrease.innerHTML = newCart.total

        reloadCart()
        reloadInventory()
      })
    }

    //This function empties the Cart
    function emptyCart() {
      const tbodyCart = document.getElementById('tbody-cart') 
      const total = document.getElementById('total')

      tbodyCart.innerHTML = ""
      total.innerHTML = '0'
      newCart = new Cart() 
    }
    

  // PAYMENT FUNCTION
    function payTotal() {
      const btnSold = document.getElementById('btn-sold')

      btnSold.addEventListener('click',()=>{
      if(newCart.articles.length<1) {
        alert('votre panier est vide')
      } else if(newUser.checkBudget(newCart)===true) {
                alert('Félicitation pour votre achat')
                emptyCart() 
              } else {
                  alert('Veuillez suprimer des articles du panier, votre budget est insuffisant')
                }
      })
    }
    