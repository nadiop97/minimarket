class Inventory {
  constructor() {
    this.articles = [
      {
        price: 2,
        quantity: 3,
        name: "Salade"
      },
      {
        price: 3,
        quantity: 2,
        name: "Pâtes"
      },
      {
        price: 3,
        quantity: 10,
        name: "Coca-cola"
      },
      {
        price: 2,
        quantity: 5,
        name: "Bn"
      },
      {
        price: 3,
        quantity: 4,
        name: "Jambon"
      },
      {
        price: 2,
        quantity: 3,
        name: "Steak"
      }
    ];
  }

  addArticleToInventory(article) {
    for(let i = 0; i<this.articles.length; i++) {
      if(this.articles[i].name === article.name) {
        this.addQuantity(i)
        return
      }
    }
    article.quantity = 1
    this.articles.push(article)
  }

  deleteArticleToInventory(article) {
      this.articles.splice(article,1)
  }

  addQuantity(articleDelete) {
    for(const article of this.articles) {
      if(article.name == articleDelete.name){
        article.quantity += 1
        console.log(article)
      }
    }
  }

  deleteQuantity(article) {
    article.quantity -= 1
  }
} 