class Cart {
  constructor() {
    this.articles = []
    this.total = 0
  }

  addArticle(article) {
    this.articles.push(article)
    this.total += article.price
    
  }

  deleteArticle(articleDelete,index) {
    this.articles.splice(index,1)
    this.total -= articleDelete.price

  }
}

// const cart = new Cart()
//  console.log(cart)
